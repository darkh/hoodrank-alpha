defmodule HoodrankAlpha.MapController do
	require Logger
  	use HoodrankAlpha.Web, :controller
  	

  	def index(conn, _params) do
    	render conn, "index.html"
  	end

  	def show(conn, %{"city" => city}) do

  		points_url = "http://www.gdanskwifi.pl/wp-content/uploads/2015/07/UM_Gdansk.csv"

		point_list = parse_cvs points_url

  		render conn, "show.html", point_list: point_list
	end

	def parse_cvs(url) do
		%HTTPoison.Response{body: body} = HTTPoison.get!(url)

		body_encoded = :unicode.characters_to_binary body, :latin1

		point_list_unparsed = String.split body_encoded, "\r\n", trim: true

		point_list = Enum.map point_list_unparsed, fn point ->
			[tag, name, lat, lon] = String.split point, ";"
			#coordinates = [lat, lon]
			#geometry = %{:type => "Point", :coordinates => coordinates}
			#properties = %{:name => name}
			#point = %{:type => "Feature", :geometry => geometry, :properties => properties}
			#JSON.encode point
			point = %{:name => name, :lat => lat, :lon => lon}
			point
		end
	end
end