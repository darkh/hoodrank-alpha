defmodule HoodrankAlpha.PageController do
  use HoodrankAlpha.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
